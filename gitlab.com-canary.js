// ==UserScript==
// @name         GitLab canary
// @namespace    http://gitlab.com/
// @version      0.2.1
// @description  Toggle gitlab.com canary
// @author       Alessio Caiazza
// @match        https://gitlab.com/*
// @grant        GM_registerMenuCommand
// @grant        GM_addStyle
// @grant        GM_log
// ==/UserScript==

(function() {
    'use strict';

    var isCanary = function(d) {
        return d.cookie.indexOf('gitlab_canary=true') >= 0;
    }

    var toggleCanary = function(){
        document.cookie='gitlab_canary='+( !isCanary(document)).toString() + ';domain=.gitlab.com;path=/;expires=' + new Date(Date.now() + 31536000000).toUTCString();
        location.reload();
    }

    GM_registerMenuCommand("Toggle canary", toggleCanary);

    if (isCanary(document)) {
        GM_log("on canary");
        GM_addStyle(".toggle-sidebar-button { border-color: #292961; }");
    }
})();

